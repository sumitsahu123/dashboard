var icmContextRoot = "/DashboardWidget";
var paths = {
	"dashboard": icmContextRoot + "/dashboard"
};
require({paths:paths});

require([
	"dojo/_base/declare",
	"dojo/_base/lang"
], function(declare, lang) {
	/**
	 * Use this function to add any global JavaScript methods your plug-in requires.
	 */

});
