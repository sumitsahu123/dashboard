package com.projak.ktkbank.dashboard.plugin;
import java.util.Locale;
import com.ibm.ecm.extension.Plugin;
import com.ibm.ecm.extension.PluginAction;

public class DashBoardPlugin extends Plugin {

	@Override
	public String getId() {
		return "DashBoardPlugin";
	}

	@Override
	public String getName(Locale arg0) {
		return "IBM BAW Custom plug-in";
	}

	@Override
	public String getVersion() {
		return "1.0.0";
	}

	@Override
	public PluginAction[] getActions() {
		return new PluginAction[] {};
	}

	@Override
	public String getScript() {
		return "DashBoardPlugin.js";
	}

	@Override
	public String getDojoModule() {
		return null;
	}

	@Override
	public String getCSSFileName() {
		return "DashBoardPlugin.css";
	}


}
