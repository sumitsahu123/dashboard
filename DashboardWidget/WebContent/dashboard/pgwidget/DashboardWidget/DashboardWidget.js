define([ "dojo/_base/declare","dojo/_base/lang", "dojo/dom-geometry", "dojo/dom-style", "dojo/dom-attr", "dojo/on",
         "dojo/query", "dojo/_base/json", "dojo/aspect", "icm/base/BasePageWidget",
	"icm/util/SearchPayload", "cc/pgwidget/ReportWidget/DashboardEventListener"],
	function(declare, lang, domGeom, domStyle, domAttr, on, query, json, aspect, BasePageWidget, SearchPayload, eventListener) {
	    return declare("dashboard.pgwidget.DashboardWidget", [ BasePageWidget ], {

		contentPaneEventListener : null,
		model : null,
		criterion : null,
		
		postCreate : function() {
		    this.inherited(arguments);
		    this.contentPaneEventListener = new eventListener(this);
		},
		
		handlerClearContent : function(){
		  console.log("handleClearContent");
		  this.clearInputValue();
		},
		
		handleICM_SearchCasesEvent : function(){
		    console.log("begin payload dump!");
		    
		    console.log("end payload dump!");
		},

		getContentListGridModules : function() {
		    var array = 0 || [];
		    array.push(DndRowMoveCopy);
		    array.push(RowContextMenu);
		    return array;
		},

		getContentListModules : function() {
		    var viewModules = 0 || [];
		    viewModules.push(ViewDetail);
		    var array = 0 || [];
		    array.push({
			moduleClass : Bar,
			top : [ [ [ {
			    moduleClass : Toolbar
			}, {
			    moduleClasses : viewModules,
			    "className" : "BarViewModules"
			} ] ] ],
			bottom : [ [ [ {
			    moduleClass : TotalCount
			} ] ] ]
		    });
		    return array;
		},

		_createGrid : function() {
		    var repo_Id = ecm.model.desktop.getDefaultRepositoryId();
		    var repository = Desktop.getRepository(repo_Id);
		    // console.log(repository);
		    /**
		     * converted to model API*
		     */
		    
		    var query = "Select * from Document";
		    
		    var queryParams = {};
		    queryParams.pageSize = 100;
		    queryParams.query = query;
		    queryParams.retrieveAllVersions = false;
		    queryParams.retrieveLatestVersion = true;
		    queryParams.repository = repository;

		    queryParams.resultsDisplay = {
			"sortBy" : "DocumentTitle",
			"sortAsc" : false,
			"columns" : [ "DocumentTitle", "Size" ],
			"honorNameProperty" : true
		    };

		    var searchQuery = new SearchQuery(queryParams);
		    searchQuery.search(function(resultSet) {
			this.contentSearchResults.setResultSet(resultSet);
		    }, null, null, null, function(error) {
			var message = new ecm.widget.dialog.MessageDialog({
			    text : "Search error::" + error
			});
			message.show();
		    });

		},
		
		handleICM_EventPayload : function(payload){
		    console.log("Dashboard Event Called");
		}
		
		
	    });
	});
