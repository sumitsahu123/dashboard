define([ "dojo/_base/declare", "dojo/_base/lang", "dojo/_base/array", "icm/base/Constants", "icm/model/Case", "ecm/LoggerMixin" ], function(declare,
	lang, array, Constants, Case, LoggerMixin) {
    return declare("cc.pgwidget.Dashboard.DashboardEventListener", [ LoggerMixin ], {
	searchTemplare : null,
	widget : null,

	constructor : function(widget) {
	    this.widget = widget;
	},

	displayPayload : function(payload) {
	    var values = [];
	    var text = JSON.stringify(payload, function(key, value) {
		if (value && typeof value === 'object') {
		    if (array.indexof(values, value) !== -1) {
			return;
		    } else {
			values.push(value);
		    }
		}
		return value || undefined;
	    }, '<br>');

	    this.widget.displayTextContent(text);
	},

	buildPayload : function(values) {

	    if (!values) {
		console.error("An invalid values is received");
		return;
	    }

	    var searchPayload = new icm.util.SearchPayload();
	    var solution = this.widget.solution;
	    var params = {};

	    params.ceQuery = "Select * from Folder";

	    params.ObjectStore = solution.getTargetOS().id;

	    var self = this;

	    this.widget.solution.retrieveCaseTypes(function(types) {
		params.caseType = types && types.length > 0 && types[0].id;
		params.solution = solution;

		searchPayload.setModel(params);
		var payload = searchPayload.getSearchPayload(function(payload) {
		    var property;
		    var reference = paylaod.detailsViewProperties[0];
		    payload.detailsViewProperties = [];
		    payload.detailsViewProperties.push[reference];

		    this.widget.onBroadcaseEvent("icm.SearchCases", payload);
		});

	    });

	},

	_eoc_ : null
    });
}

);