define([ "dojo/_base/declare", "dojo/text!./templates/DashboardWidget.html", "dijit/form/Button", "ecm/model/SearchQuery",
	"ecm/widget/listView/ContentList", "ecm/widget/listView/gridModules/RowContextMenu", "ecm/widget/listView/modules/Toolbar2",
	"ecm/widget/listView/modules/DocInfo", "ecm/widget/listView/gridModules/DndRowMoveCopy",
	"ecm/widget/listView/gridModules/DndFromDesktopAddDoc", "ecm/widget/listView/modules/Bar", "ecm/widget/listView/modules/ViewDetail",
	"ecm/widget/listView/modules/ViewMagazine", "ecm/widget/listView/modules/ViewFilmStrip", "ecm/widget/listView/modules/TotalCount",
	"ecm/model/Desktop" , "icm/base/_BaseWidget"],
	function(declare, template, Button, SearchQuery, ContentList, RowContextMenu, Toolbar, DocInfo,
		DndRowMoveCopy, DndFromDesktopAddDoc, Bar, ViewDetail, ViewMagazine, ViewFilmStrip, TotalCount, Desktop, _BaseWidget){
    return declare("dashboard.pgwidget.DashboardWidget.dijit.DashboardWidgetContentPane", [_BaseWidget], {
	
	templateString : template,
	widgetsInTemplate : true,
	fieldsInForm : null,
	
	createSearchFields : function(solution){
	    solution.retrieveAttributeDefinitions(lang.hitch(this, this._buildSearchFields));
	},
	
    	_buildSearchFields : function(attrs){
    	    
    	},
    	
    	postCreate : function(){
    	    this.inherited(arguments);
	    aspect.after(Desktop, "onLogin", lang.hitch(this, function() {
		this.contentSearchResults.setContentListModules(this.getContentListModules());
		this.contentSearchResults.setGridExtensionModules(this.getContentListGridModules());
	    }));
    	}
	
    } );
});